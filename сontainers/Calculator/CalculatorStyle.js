
const CalcStyle = {
    container: {
        paddingTop: 50,
        paddingBottom: 20,
        backgroundColor: "#f9f9f9",
    },
    showNumbers: {
        width: 400,
        borderWidth: 1,
        borderColor: "#212121",
        height: 60,
        textAlign: 'center',
        fontSize: 25,
        alignSelf: 'center'
    },
    value: {
        textAlign: 'center',
        fontSize: 32,
        color: '#212121',
        alignItems: 'center'
    },
    keypad : {
        display: 'flex',
        flexWrap: 'wrap',
        width: 300,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    numButton: {
        width: '33%',
    },
    num: {
        margin: 10,
        backgroundColor: "#316ad3",
        height: 60,
        width: 80,
        borderRadius: 10,
        justifyContent: 'center',
    },
    buttonValue: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 25,
    },
};

export default CalcStyle;