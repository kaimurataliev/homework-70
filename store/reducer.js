import * as actions from './actions';

const initialState = {
    numbers: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.PUT_NUMBER:
            return {...state, numbers: state.numbers += action.value};

        case actions.DELETE_NUMBER:
            return {...state, numbers: state.numbers.slice(0, -1)};

        case actions.GET_RESULT:
            return {...state, numbers: String(eval(state.numbers))};

        case actions.CLEAR_FIELD:
            return {...state, numbers: ''};

        default:
            return state;
    }
};

export default reducer;