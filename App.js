import React from 'react';
import reducer from './store/reducer';
import {createStore} from 'redux';
import { Provider } from 'react-redux';

import Calculator from './сontainers/Calculator/Calculator';

const store = createStore(reducer);

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
          <Calculator/>
        </Provider>
    );
  }
}


