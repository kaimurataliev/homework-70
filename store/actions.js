
export const PUT_NUMBER = "PUT_NUMBER";
export const DELETE_NUMBER = "DELETE_NUMBER";
export const GET_RESULT = "GET_RESULT";
export const CLEAR_FIELD = "CLEAR_FIELD";

export const putValue = (value) => {
    return {type: PUT_NUMBER, value}
};

export const deleteNumber = () => {
    return {type: DELETE_NUMBER};
};

export const getResult = () => {
    return {type: GET_RESULT};
};

export const clearField = () => {
    return {type: CLEAR_FIELD};
};


