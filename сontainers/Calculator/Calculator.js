import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import CalcStyle from './CalculatorStyle';
import { putValue, deleteNumber, getResult, clearField } from '../../store/actions';



class Calculator extends Component {

    getValue = (value) => {
        if (value === '=') {
            this.props.getResult();
        } else if (value === '<') {
            this.props.deleteNumber();
        } else if (value === 'clear') {
            this.props.clearField();
        } else {
            this.props.putValue(value)
        }

    };

    render() {

        const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", '.', '*', '/', '-', '+', '<', '=', 'clear'];

        return (
            <ScrollView style={CalcStyle.container}>

                <View style={CalcStyle.showNumbers}>
                    <Text style={CalcStyle.value}>{this.props.numbers}</Text>
                </View>

                <View style={CalcStyle.keypad}>
                    {numbers.map((key, index) => {
                        return (
                            <View key={index} style={CalcStyle.numButton}>
                                <TouchableOpacity style={CalcStyle.num} onPress={() => this.getValue(key)} title={key}>
                                    <Text style={CalcStyle.buttonValue} >{key}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </View>

            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        numbers: state.numbers
    }
};

const mapDispatchToProps = dispatch => {
    return {
        putValue: (value) => dispatch(putValue(value)),
        getResult: () => dispatch(getResult()),
        deleteNumber: () => dispatch(deleteNumber()),
        clearField: () => dispatch(clearField())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);